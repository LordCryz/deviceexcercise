package enviorment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.cj.xdevapi.Statement;

public class DatabaseConnector {

    private static DatabaseConnector dbIsntance;
    private static Connection con ;
    private static Statement stmt;


    private DatabaseConnector() {
      // private constructor //
    }

    public static DatabaseConnector getInstance(){
    if(dbIsntance==null){
        dbIsntance= new DatabaseConnector();
    }
    return dbIsntance;
    }

    public  Connection getConnection(){
//                "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234")) {
        if(con==null){
            try {
                String host = "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#";
                String username = "root";
                String password = "1234";
                con = DriverManager.getConnection( host, username, password );
            } catch (SQLException e) {
                System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            } catch (Exception e) {
                e.printStackTrace();
            } 
        }

        return con;
    }
}